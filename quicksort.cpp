/*
 * Usage: quicksort 12 13 5 3 54 327 ... n
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <ctime>

/*
 * Swap two values within the vector
 */
void swap(int *firstValue, int *secondValue, unsigned int &swaps){
  swaps++;
  int tempValue = *firstValue;
  *firstValue = *secondValue;
  *secondValue = tempValue;
}

/*
 * Partitioning code
 */
int partition(std::vector <int> &list, int low, int high, unsigned int &swaps, unsigned int &partitions){
  partitions++;
  int p = list[low];
  int i = low;
  int j = high+1;
  do{
    do{i++;}while(list[i] < p);
    do{j--;}while(list[j] > p);
    swap(&list[i], &list[j], swaps);
  }while(i < j);
  swap(&list[i], &list[j], swaps);
  swap(&list[low], &list[j], swaps);
  return j;
}

/*
 * Quick Sort algorithm
 */
void quicksort(std::vector <int> &list, int low, int high, unsigned int &swaps, unsigned int &partitions, unsigned int &sorts){
  sorts++;
  if(low < high){
    int s = partition(list, low, high, swaps, partitions);
    quicksort(list, low, s-1, swaps, partitions, sorts);
    quicksort(list, s+1, high, swaps, partitions, sorts);
  }
}

/*
 * Print all the values in the vector (list)
 */
void printVector(std::vector <int> list){
  std::cout << "Output     : ";
  for(int i = 0; i < list.size(); i++)
    std::cout << list[i] << " ";
  std::cout << std::endl;
}

/*
 * Driver code
 */
int main(int argc, char *argv[]){
  unsigned int swaps = 0;
  unsigned int partitions = 0;
  unsigned int sorts = 0;
  std::vector <int> list; // List of numbers to sort

  // Get numbers from user and add them to the list
  for(int i = 0; i < argc; i++)
    if(isdigit(*argv[i]))
      list.push_back(atoi(argv[i]));

  std::cout << "Input      : ";
  for(int i = 0; i < list.size(); i++) std::cout << list[i] << " ";
  std::cout << std::endl;

  // Run the algorithm and print the resulting list
  clock_t begin_time = clock();
  quicksort(list, 0, list.size()-1, swaps, partitions, sorts);
  clock_t end_time = clock();
  printVector(list);
  std::cout << "Sorts      : " << sorts << std::endl;
  std::cout << "Partitions : " << partitions << std::endl;
  std::cout << "Swaps      : " << swaps << std::endl;
  std::cout << "Time       : " << (end_time - begin_time) / (double) CLOCKS_PER_SEC << std::endl;
  std::cout << std::endl;

  exit(EXIT_SUCCESS);
}
